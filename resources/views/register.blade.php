<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>form</title>
</head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <form action = "/welcome" method = "POST">
            @csrf
            <div>First name:</div>
            <br>
            <input type="name" name="First_Name">
            <br>
            <br>
            <div>Last Name:</div>
            <br>
            <input type="name" name="Last_Name">
            <br>
            <br>
            <div>Gender:</div>
            <br>
            <div>
            <input type="radio" name="radioButton" value="Male"><label>Male</label>
            </div>
            <div>
            <input type="radio" name="radioButton" value="Female"><label>Female</label>
            </div>
            <div>
            <input type="radio" name="radioButton" value="Other"><label>Other</label>
            </div>
            <br>
            <br>
            <div>Nationality:</div>
            <br>
            <select>
                <option>Indonesian</option>
                <option>Singaporean</option>
                <option>Malaysian</option>
                <option>Australian</option>
            </select>
            <div>
            <p>Language Spoken:</p>
            <input type = "checkbox" value = "Bahasa Indonesia"><label>Bahasa Indonesia</label><br>    
            <input type = "checkbox" value = "English"><label>English</label><br>    
            <input type = "checkbox" value = "Other"><label>Other</label><br>    
            </div>
            <div>
                <p>Bio:</p>
                <textarea cols="25" rows="10"></textarea>
            </div>
            <div>
                <input type = "submit" value = "Sign Up"> 
            </div>
        </form>  
    </body>
</html>